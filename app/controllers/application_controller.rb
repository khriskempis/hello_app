class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    render html: 
      "<h1>hello</h1>
      <p>this is a paragraph </p>"
  end 

  def goodbye
    render html: "goodbye world"
  end

end
